<?php
/*
Plugin Name: webselect
Description: webselect PHPDI
Author: IdeaInYou
Text Domain: webselect
Domain Path: https://www.ideainyou.com
Version: 1.0.0
*/
if (!defined('ABSPATH')) exit;
require_once ABSPATH . 'wp-content/plugins/elementor/elementor.php';
require_once __DIR__ . '/namespaces.php';
require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/config.php';