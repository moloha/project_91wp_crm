<?php

namespace Webselect\Elementor;

use Webselect\Elementor\Helper\ElementorHelper;
use Webselect\Elementor\ElementorWidgets\ElementorWidgets;

/**
 * Class Elementor
 * @package webselect\Elementor
 */
class Elementor
{
    /**
     * @var ElementorHelper
     */
    protected $elementorHelper;
    /**
     * @var ElementorWidgets
     */
    protected $elementorWidgets;

    /**
     *
     */
    const CATEGORY = 'webselect';
    /**
     *
     */
    const CATEGORY_TITLE = 'webselect';

    /**
     * Elementor constructor.
     * @param ElementorHelper $elementorHelper
     * @param ElementorWidgets $elementorWidgets
     */
    public function __construct(
        ElementorHelper $elementorHelper,
        ElementorWidgets $elementorWidgets
    )
    {
        $this->elementorHelper = $elementorHelper;
        $this->elementorWidgets = $elementorWidgets;
        $this->init();
    }

    /**
     *
     */
    function init()
    {
        $this->elementorActions();
    }

    /**
     *
     */
    function elementorActions()
    {
        add_action('elementor/init', [$this, 'registerCategory']);
        add_action('elementor/widgets/widgets_registered', [$this->elementorWidgets, 'registerWidgets']);
//        add_action( 'wp_enqueue_scripts', [$this, 'disableElementorStyles']);
//        add_action( 'elementor/frontend/after_register_styles', [$this, 'afterRegisterStyles']);
//        add_filter( 'elementor/frontend/print_google_fonts', '__return_false');
    }

    /**
     *
     */
    function registerCategory()
    {
        $category = [
            'title' => __(self::CATEGORY_TITLE, self::CATEGORY),
            'icon' => 'fa fa-plug'
        ];
        $this->elementorHelper->getElementorPlugin()->elements_manager->add_category(
            self::CATEGORY,
            $category
        );
    }

    /**
     *
     */
    function disableElementorStyles() {
        wp_deregister_style( 'elementor-global' );
        wp_deregister_style( 'elementor-common' );
        wp_deregister_style( 'elementor-icons' );
        wp_deregister_style( 'elementor-animations' );
    }

    function afterRegisterStyles() {
        foreach( [ 'solid', 'regular', 'brands' ] as $style ) {
            wp_deregister_style( 'elementor-icons-fa-' . $style );
        }
    }
}