<?php

namespace Webselect\Elementor\Helper;
use \Elementor\Plugin;
/**
 * Class ElementorHelper
 * @package webselect\Helper
 */
class ElementorHelper
{

    /**
     * @var Plugin|null
     */
    private $elementorPlugin;
    /**
     * @var Plugin|null
     */
    private $elementorPluginInstance;

    /**
     * ElementorHelper constructor.
     */
    public function __construct()
    {
        $this->elementorPlugin = Plugin::$instance;
        $this->elementorPluginInstance = Plugin::instance();
    }


    /**
     * @return \Elementor\Plugin|Plugin|null
     */
    function getElementorPlugin()
    {
        return $this->elementorPlugin;
    }

    /**
     * @return \Elementor\Plugin|Plugin|null
     */
    function getElementorPluginInstance()
    {
        return $this->elementorPluginInstance;
    }

}