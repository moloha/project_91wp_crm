<?php

namespace Webselect\Elementor\ElementorWidgets\Widgets;

use Webselect\Elementor\ElementorWidgets\ElementorWidget;


class ImageSolution extends ElementorWidget {

	protected $classes;

	function __construct( $data = [], $args = null ) {
		$this->classes = $args;

		$this->setName( 'Image Solution' );

		//@ToDo this not working
		$this->setTemplate( 'elementorTemplates/image-solution' );

		parent::__construct( $data, $args );
	}


	protected function _register_controls() {

		$this->addTab( 'Image Solution', function () {








//			=======================================================================
//			$this->imageField('My image field', 'my_image_field');

//			$this->textField('My text field', 'my_text_field', 'Default text');

//			$this->urlField('My url field', 'my_url_field');

			$this->WYSIWYG('My editor field', 'my_editor_field');




//			========================================================================







			$this->chooseField( 'Position', 'position', [
				'left'   => [
					'title' => 'left',
					'icon'  => 'fa fa-align-left',
				],
				'center' => [
					'title' => 'center',
					'icon'  => 'fa fa-align-center',
				],
				'right'  => [
					'title' => 'right',
					'icon'  => 'fa fa-align-right',
				],
			], 'center' );
		} );
	}
}
