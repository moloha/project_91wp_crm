<?php

namespace Webselect\Elementor\ElementorWidgets\Widgets;

use Webselect\Elementor\ElementorWidgets\ElementorWidget;


class NotesForTable extends ElementorWidget {

	protected $classes;

	function __construct( $data = [], $args = null ) {
		$this->classes = $args;

		$this->setName( 'Notes For Table' );

		$this->setTemplate( 'elementorTemplates/notes-for-table' );

		parent::__construct( $data, $args );
	}


	protected function _register_controls() {

		$this->addTab( 'Notes For Table', function () {

			$this->heading('My notes');



			$this->textareaField( 'Sub Title', 'sub_notetitle', 'description' );

//			$this->textField('Notes For Table', 'notes_for_table', 'Default text');
//			$this->textareaField( 'Notes For Table', 'notes_for_table', 'Default text' );




			$this->repeater('My notes fields', 'my_notes1_fields', function ($repeater) {

				$this->textareaFieldRepeater($repeater, 'My text field', 'sub_notetitle', 'Default text');

//				$this->textFieldRepeater($repeater, 'Notes For Table', 'notes_for_table', 'Default text');


			});
		} );
	}
}
