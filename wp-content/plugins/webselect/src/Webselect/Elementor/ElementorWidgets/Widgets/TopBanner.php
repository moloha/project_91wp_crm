<?php
namespace Webselect\Elementor\ElementorWidgets\Widgets;

use Webselect\Elementor\ElementorWidgets\ElementorWidget;


class TopBanner extends ElementorWidget
{

    protected $classes;

    function __construct($data = [], $args = null)
    {
        $this->classes = $args;

        $this->setName('TopBanner');

        $this->setTemplate('elementorTemplates/top-banner');

        parent::__construct($data, $args);
    }


    protected function _register_controls()
    {

        $this->addTab('TopBanner', function () {

            $this->textField('Sub title', 'sub_title', 'We’re a full service provider');

            $this->textField('Title', 'title', 'Magento platform and we can help you today');

            $this->textareaField('Description', 'description', 'We support and encourage the thirst for innovative ideas. Our client’s growth success is our top priority');

            $this->textField('Button title', 'button_title', 'Our Services');

            $this->urlField('Button', 'button');

            $this->textareaField('Slogan', 'slogan', 'We unite all ideas of our customer');

        });
    }
}
