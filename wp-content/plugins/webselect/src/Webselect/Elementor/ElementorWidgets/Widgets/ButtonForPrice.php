<?php

namespace Webselect\Elementor\ElementorWidgets\Widgets;

use Webselect\Elementor\ElementorWidgets\ElementorWidget;


class ButtonForPrice extends ElementorWidget {

	protected $classes;

	function __construct( $data = [], $args = null ) {
		$this->classes = $args;

		$this->setName( 'Button For Price' );

		//@ToDo this not working
		$this->setTemplate( 'elementorTemplates/button-for-price' );

		parent::__construct( $data, $args );
	}


	protected function _register_controls() {

		$this->addTab( 'Button For Price', function () {

			$this->textField('Button title', 'button_title', 'Our Services');

			$this->urlField('Button', 'button');

			$this->chooseField( 'Position', 'position', [
				'left'   => [
					'title' => 'left',
					'icon'  => 'fa fa-align-left',
				],
				'center' => [
					'title' => 'center',
					'icon'  => 'fa fa-align-center',
				],
				'right'  => [
					'title' => 'right',
					'icon'  => 'fa fa-align-right',
				],
			], 'center' );
		} );
	}
}
