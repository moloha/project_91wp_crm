<?php

namespace Webselect\Elementor\ElementorWidgets\Widgets;

use Webselect\Elementor\ElementorWidgets\ElementorWidget;


class PriceTab extends ElementorWidget {

	protected $classes;

	function __construct( $data = [], $args = null ) {
		$this->classes = $args;

		$this->setName( 'Price Tab' );

		$this->setTemplate( 'elementorTemplates/price-tab' );

		parent::__construct( $data, $args );
	}


	protected function _register_controls() {

		$this->addTab( 'Price Tab', function () {

			$this->heading('My Prices');

			$this->textField( 'Title', 'title', 'Package name.' );


			$this->textField( 'Price', 'price', '9,90 €' );

			$this->textareaField( 'Sub Title', 'sub_pricetitle', 'description' );




			$this->repeater('My table fields', 'my_table_fields', function ($repeater) {


				$this->textareaFieldRepeater($repeater, 'My text field', 'sub_pricetitle', 'Default text');


			});
		} );
	}
}
