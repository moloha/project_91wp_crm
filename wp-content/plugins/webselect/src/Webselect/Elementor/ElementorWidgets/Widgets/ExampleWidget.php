<?php

namespace Webselect\Elementor\ElementorWidgets\Widgets;

use Webselect\Elementor\ElementorWidgets\ElementorWidget;


class ExampleWidget extends ElementorWidget
{
    protected $classes;

    function __construct($data = [], $args = null)
    {
        $this->classes = $args;

        $this->setName('ExampleWidget');

        $this->setTemplate('elementorTemplates/example-widget');

        parent::__construct($data, $args);
    }


    protected function _register_controls()
    {
        $this->addTab('General content', function () {

            $this->selectField('My select name', 'my_select_id', [
                '1' => 'Option 1',
                '2' => 'Option 2',
                '3' => 'Option 3'
            ], '1');

            $this->select2Field('My select2 name', 'my_select2_id', [
                '1' => 'Option 1',
                '2' => 'Option 2',
                '3' => 'Option 3'
            ], '2');

            $this->numberField('My number field', 'my_number_field', 0);

            $this->switchField('My switch field', 'my_switch_field');

            $this->textareaField('My textarea field', 'my_textarea_field', 'Default text');

            $this->colorField('My color field', 'my_color_field', '#ffffff');

            $this->heading('My heading');

            $this->chooseField('My choose field', 'my_choose_field', [
                'left' => [
                    'title' => '1',
                    'icon' => 'fa fa-align-left',
                ],
                'center' => [
                    'title' => '2',
                    'icon' => 'fa fa-align-center',
                ],
                'right' => [
                    'title' => '3',
                    'icon' => 'fa fa-align-right',
                ],
            ], 'center');

            $this->imageField('My image field', 'my_image_field');

            $this->textField('My text field', 'my_text_field', 'Default text');

            $this->urlField('My url field', 'my_url_field');

            $this->WYSIWYG('My editor field', 'my_editor_field');



            $this->repeater('My repeater', 'my_repeater_id', function ($repeater) {

                $this->imageFieldRepeater($repeater, 'My image field', 'my_image_field');

                $this->textFieldRepeater($repeater, 'My text field', 'my_text_field', 'Default text');

                $this->selectFieldRepeater($repeater, 'My select name', 'my_select_id', [
                    '1' => 'Option 1',
                    '2' => 'Option 2',
                    '3' => 'Option 3'
                ], '1');

                $this->numberFieldRepeater($repeater, 'My number field', 'my_number_field', 0);

                $this->select2FieldRepeater($repeater, 'My select2 name', 'my_select2_id', [
                    '1' => 'Option 1',
                    '2' => 'Option 2',
                    '3' => 'Option 3'
                ], '2');

                $this->switchFieldRepeater($repeater,'My switch field', 'my_switch_field');

                $this->textareaFieldRepeater($repeater,'My textarea field', 'my_textarea_field', 'Default text');

                $this->chooseFieldRepeater($repeater,'My choose field', 'my_choose_field', [
                    'left' => [
                        'title' => '1',
                        'icon' => 'fa fa-align-left',
                    ],
                    'center' => [
                        'title' => '2',
                        'icon' => 'fa fa-align-center',
                    ],
                    'right' => [
                        'title' => '3',
                        'icon' => 'fa fa-align-right',
                    ],
                ], 'center');

                $this->urlFieldRepeater($repeater,'My url field', 'my_url_field');

                $this->WYSIWYGRepeater($repeater,'My editor field', 'my_editor_field');

                $this->colorFieldRepeater($repeater,'My color field', 'my_color_field', '#ffffff');

                //@ToDo this not working
                $this->getRepeaterInside($repeater);
            });
        });
    }

    //@ToDo this not working
    function getRepeaterInside($r) {
        $this->repeaterInside($r, 'My repeater', 'my_repeater_id_2', function ($repeater2) {

            $this->imageFieldRepeater($repeater2, 'My image field 2', 'my_image_field');

            $this->textFieldRepeater($repeater2, 'My text field 2', 'my_text_field', 'Default text');

            $this->selectFieldRepeater($repeater2, 'My select name 2', 'my_select_id', [
                '1' => 'Option 1',
                '2' => 'Option 2',
                '3' => 'Option 3'
            ], '1');

            $this->numberFieldRepeater($repeater2, 'My number field 2', 'my_number_field', 0);

            $this->select2FieldRepeater($repeater2, 'My select2 name 2', 'my_select2_id', [
                '1' => 'Option 1',
                '2' => 'Option 2',
                '3' => 'Option 3'
            ], '2');

            $this->switchFieldRepeater($repeater2,'My switch field 2', 'my_switch_field');

            $this->textareaFieldRepeater($repeater2,'My textarea field 2', 'my_textarea_field', 'Default text');

            $this->chooseFieldRepeater($repeater2,'My choose field 2', 'my_choose_field', [
                'left' => [
                    'title' => '1',
                    'icon' => 'fa fa-align-left',
                ],
                'center' => [
                    'title' => '2',
                    'icon' => 'fa fa-align-center',
                ],
                'right' => [
                    'title' => '3',
                    'icon' => 'fa fa-align-right',
                ],
            ], 'center');

            $this->urlFieldRepeater($repeater2,'My url field 2', 'my_url_field');

            $this->WYSIWYGRepeater($repeater2,'My editor field 2', 'my_editor_field');

            $this->colorFieldRepeater($repeater2,'My color field 2', 'my_color_field', '#ffffff');

        });
    }

}
