<?php

namespace Webselect\Elementor\ElementorWidgets\Widgets;

use Webselect\Elementor\ElementorWidgets\ElementorWidget;


class LeftSolution extends ElementorWidget {

	protected $classes;

	function __construct( $data = [], $args = null ) {
		$this->classes = $args;

		$this->setName( 'Left Solution' );

		//@ToDo this not working
		$this->setTemplate( 'elementorTemplates/left-solution' );

		parent::__construct( $data, $args );
	}


	protected function _register_controls() {

		$this->addTab( 'Left Solution', function () {

			$this->textField( 'Title', 'title', 'Contact management.' );

			$this->textField( 'Sub Title', 'sub_title', 'Quick and easy.' );

			$this->textareaField( 'Description', 'description', 'Use intelligent wizards to digitalize your contacts quickly and easily. SmartWe can capture your contact information completely independently of the data source, regardless of whether from e-mail, documents, business cards, or web pages. And then save the correct information to the right place. So it’s ready for you to access at a moment’s notice.

The radial menu keeps your customer at the center of your focus, with all related and important information around them, and lets you access them in the quick view.' );


//			=======================================================================
//			$this->imageField('My image field', 'my_image_field');
//
//			$this->textField('My text field', 'my_text_field', 'Default text');
//
//			$this->urlField('My url field', 'my_url_field');
//
//			$this->WYSIWYG('My editor field', 'my_editor_field');


//			========================================================================


			$this->textField( 'Button title', 'button_title', 'Our Services' );

			$this->urlField( 'Button', 'button' );

			$this->chooseField( 'Position', 'position', [
				'left'   => [
					'title' => 'left',
					'icon'  => 'fa fa-align-left',
				],
				'center' => [
					'title' => 'center',
					'icon'  => 'fa fa-align-center',
				],
				'right'  => [
					'title' => 'right',
					'icon'  => 'fa fa-align-right',
				],
			], 'center' );
		} );
	}
}
