<?php

namespace Webselect\Elementor\ElementorWidgets;

use Elementor\Controls_Manager;
use Elementor\Utils;
use Elementor\Widget_Base;

class ElementorWidget extends Widget_Base
{

    protected $widgetName;
    protected $widgetTitle;
    protected $template;

    function __construct($data = [], $args = null)
    {
        parent::__construct($data, $args);
    }

    function get_name()
    {
        return __($this->widgetName, PROJECT_KEY);
    }

    public function get_title()
    {
        return __($this->widgetName, PROJECT_KEY);
    }

    function setName($name)
    {
        $this->widgetName = $name;
    }

    public function get_icon()
    {
        return 'eicon-post-list';
    }

    /**
     * @return array|string[]
     */
    public function get_categories()
    {
        return [PROJECT_KEY];
    }

    protected function render()
    {
        get_template_part($this->template,'', ['settings' => $this->get_settings_for_display()]);
    }

    public function setTemplate($name)
    {
        $this->template = $name;
    }

    public function addTab($tabName, $data)
    {
        $this->start_controls_section(
            $this->generateId($tabName),
            [
                'label' => __($tabName, PROJECT_KEY),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $data();

        $this->endTab();
    }

    public function imageField($fieldName, $fieldId)
    {
        $this->add_control(
            $fieldId,
            $this->imageFieldData($fieldName)
        );
    }

    public function imageFieldRepeater($repeater, $fieldName, $fieldId)
    {
        $repeater->add_control(
            $fieldId,
            $this->imageFieldData($fieldName)
        );
    }

    public function imageFieldData($fieldName) {
        return [
            'label' => __($fieldName, PROJECT_KEY),
            'type' => Controls_Manager::MEDIA,
            'default' => [
                'url' => Utils::get_placeholder_image_src(),
            ]
        ];
    }

    public function textField($fieldName, $fieldId, $default = false)
    {
        $data = $this->textFieldData($fieldName);

        if ($default) {
            $data['default'] = $default;
        }

        $this->add_control(
            $fieldId,
            $data
        );
    }

    public function textFieldRepeater($repeater, $fieldName, $fieldId, $default = false)
    {
        $data = $this->textFieldData($fieldName);

        if ($default) {
            $data['default'] = $default;
        }

        $repeater->add_control(
            $fieldId,
            $data
        );
    }

    public function textFieldData($fieldName) {
        return [
            'label' => __($fieldName, PROJECT_KEY),
            'type' => Controls_Manager::TEXT,
        ];
    }

    public function selectField($fieldName, $fieldId, $options, $selected = false)
    {
        $data = $this->selectFieldData($fieldName, $options);

        if ($selected) {
            $data['default'] = $selected;
        }

        $this->add_control(
            $fieldId,
            $data
        );
    }

    public function selectFieldRepeater($repeater, $fieldName, $fieldId, $options, $selected = false)
    {
        $data = $this->selectFieldData($fieldName, $options);

        if ($selected) {
            $data['default'] = $selected;
        }

        $repeater->add_control(
            $fieldId,
            $data
        );
    }

    public function selectFieldData($fieldName, $options) {
        return [
            'label' => __($fieldName, PROJECT_KEY),
            'type' => Controls_Manager::SELECT,
            'options' => $options
        ];
    }


    public function numberField($fieldName, $fieldId, $default = false)
    {
        $data = $this->numberFieldData($fieldName);

        if ($default) {
            $data['default'] = $default;
        }

        $this->add_control(
            $fieldId,
            $data
        );
    }

    public function numberFieldRepeater($repeater, $fieldName, $fieldId, $default = false)
    {
        $data = $this->numberFieldData($fieldName);

        if ($default) {
            $data['default'] = $default;
        }

        $repeater->add_control(
            $fieldId,
            $data
        );
    }

    public function numberFieldData($fieldName) {
        return [
            'label' => __($fieldName, PROJECT_KEY),
            'type' => Controls_Manager::NUMBER
        ];
    }

    public function select2Field($fieldName, $fieldId, $options, $selected = false)
    {
        $data = $this->select2FieldData($fieldName, $options);

        if ($selected) {
            $data['default'] = $selected;
        }

        $this->add_control(
            $fieldId,
            $data
        );
    }

    public function select2FieldRepeater($repeater, $fieldName, $fieldId, $options, $selected = false)
    {
        $data = $this->select2FieldData($fieldName, $options);

        if ($selected) {
            $data['default'] = $selected;
        }

        $repeater->add_control(
            $fieldId,
            $data
        );
    }

    public function select2FieldData($fieldName, $options) {
        return [
            'label' => __($fieldName, PROJECT_KEY),
            'type' => Controls_Manager::SELECT,
            'options' => $options
        ];
    }

    public function switchField($fieldName, $fieldId)
    {
        $this->add_control(
            $fieldId,
            $this->switchFieldData($fieldName)
        );
    }

    public function switchFieldRepeater($repeater, $fieldName, $fieldId)
    {
        $repeater->add_control(
            $fieldId,
            $this->switchFieldData($fieldName)
        );
    }

    public function switchFieldData($fieldName) {
        return [
            'label' => __($fieldName, PROJECT_KEY),
            'type' => Controls_Manager::SWITCHER,
            'label_on' => __('True', PROJECT_KEY),
            'label_off' => __('False', PROJECT_KEY),
            'return_value' => true,
            'default' => false,
        ];
    }

    public function textareaField($fieldName, $fieldId, $default = false)
    {
        $data = $this->textareaFieldData($fieldName);

        if ($default) {
            $data['default'] = $default;
        }

        $this->add_control(
            $fieldId,
            $data
        );
    }

    public function textareaFieldRepeater($repeater, $fieldName, $fieldId, $default = false)
    {
        $data = $this->textareaFieldData($fieldName);

        if ($default) {
            $data['default'] = $default;
        }

        $repeater->add_control(
            $fieldId,
            $data
        );
    }

    public function textareaFieldData($fieldName) {
        return [
            'label' => __($fieldName, PROJECT_KEY),
            'type' => Controls_Manager::TEXTAREA
        ];
    }

    public function heading($text, $separator = 'before')
    {
        $this->add_control(
            $this->generateId($text),
            [
                'label' => __($text, PROJECT_KEY),
                'type' => Controls_Manager::HEADING,
                'separator' => $separator,
            ]
        );
    }

    public function chooseField($fieldName, $fieldId, $options, $selected = false)
    {
        $data = $this->chooseFieldData($fieldName, $options);

        if ($selected) {
            $data['default'] = $selected;
        }

        $this->add_control(
            $fieldId,
            $data
        );
    }

    public function chooseFieldRepeater($repeater, $fieldName, $fieldId, $options, $selected = false)
    {
        $data = $this->chooseFieldData($fieldName, $options);

        if ($selected) {
            $data['default'] = $selected;
        }

        $repeater->add_control(
            $fieldId,
            $data
        );
    }

    public function chooseFieldData($fieldName, $options) {
        return [
            'label' => __($fieldName, PROJECT_KEY),
            'type' => Controls_Manager::CHOOSE,
            'options' => $options,
            'default' => 'center',
        ];
    }

    public function urlField($fieldName, $fieldId)
    {
        $this->add_control(
            $fieldId,
            $this->urlFieldData($fieldName)
        );
    }

    public function urlFieldRepeater($repeater, $fieldName, $fieldId)
    {
        $repeater->add_control(
            $fieldId,
            $this->urlFieldData($fieldName)
        );
    }

    public function urlFieldData($fieldName) {
        return [
            'label' => __($fieldName, PROJECT_KEY),
            'type' => Controls_Manager::URL
        ];
    }

    public function WYSIWYG($fieldName, $fieldId, $default = false) {
        $data = $this->WYSIWYGData($fieldName);

        if($default) {
            $data['default'] = $default;
        }

        $this->add_control(
            $fieldId,
            $data
        );

    }

    public function WYSIWYGRepeater($repeater, $fieldName, $fieldId, $default = false) {
        $data = $this->WYSIWYGData($fieldName);

        if($default) {
            $data['default'] = $default;
        }

        $repeater->add_control(
            $fieldId,
            $data
        );

    }

    public function WYSIWYGData($fieldName) {
        return [
            'label' => __( $fieldName, PROJECT_KEY ),
            'type' => Controls_Manager::WYSIWYG,
            'default' => __( 'List Content' , 'plugin-domain' ),
            'show_label' => false,
        ];
    }

    public function colorField($fieldName, $fieldId, $default = false)
    {
        $data = $this->colorFieldData($fieldName);

        if ($default) {
            $data['default'] = $default;
        }

        $this->add_control(
            $fieldId,
            $data
        );
    }

    public function colorFieldRepeater($repeater, $fieldName, $fieldId, $default = false)
    {
        $data = $this->colorFieldData($fieldName);

        if ($default) {
            $data['default'] = $default;
        }

        $repeater->add_control(
            $fieldId,
            $data
        );
    }

    public function colorFieldData($fieldName) {
        return [
            'label' => __($fieldName, PROJECT_KEY),
            'type' => Controls_Manager::COLOR
        ];
    }

    public function repeater($name, $id ,$fields)
    {
        $$id = new \Elementor\Repeater();

        $fields($$id);

        $this->add_control(
            $id,
            [
                'label' => __( $name, PROJECT_KEY ),
                'type' => Controls_Manager::REPEATER,
                'fields' => $$id->get_controls()
            ]
        );
    }

    public function repeaterInside($repeater, $name, $id ,$fields)
    {
        $$id = new \Elementor\Repeater();

        $fields($$id);

        $repeater->add_control(
            $id,
            [
                'label' => __( $name, PROJECT_KEY ),
                'type' => Controls_Manager::REPEATER,
                'fields' => $$id->get_controls()
            ]
        );
    }

    public function generateId($name) {
        return str_replace(' ', '_', strtolower($name));
    }

    public function endTab()
    {
        $this->end_controls_section();
    }
}
