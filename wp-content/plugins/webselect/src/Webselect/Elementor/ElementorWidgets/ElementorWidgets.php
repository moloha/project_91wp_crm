<?php

namespace Webselect\Elementor\ElementorWidgets;

use Webselect\Elementor\Helper\ElementorHelper;
use Webselect\Elementor\Helper\Widgets\someTest;

/**
 * Class ElementorWidgets
 * @package webselect\Elementor\ElementorWidgets
 */
class ElementorWidgets
{
    /**
     * @var ElementorHelper
     */
    protected $elementorHelper;
    /**
     * @var someTest
     */
    protected $test;

    /**
     * @var \webselect\Elementor\Helper\Widgets\someTest[][]
     */
    protected $classConnector;
    /**
     *
     */
    const WIDGETS_FOLDER = __DIR__ . '/Widgets/';
    /**
     *
     */
    const IGNORED = [
        '.',
        '..',
        'ExampleWidget.php'
    ];

    /**
     * ElementorWidgets constructor.
     * @param ElementorHelper $elementorHelper
     * @param someTest $test
     */
    function __construct(
        ElementorHelper $elementorHelper,
        someTest $test
    )
    {
        $this->elementorHelper = $elementorHelper;
        $this->test = $test;

        $this->addClassConnector();
    }

    function addClassConnector()
    {
        $this->classConnector = [
            'ExampleWidget' => [
                'test' => $this->test
            ]
        ];
    }

    /**
     *
     */
    function registerWidgets()
    {
        if (defined('ELEMENTOR_PATH') && class_exists('Elementor\Widget_Base')) {
            $this->requireWidgets();
        }
    }

    /**
     *
     */
    function requireWidgets()
    {
        $widgets = scandir(self::WIDGETS_FOLDER);
        foreach ($widgets as $widget) {
            if (!in_array($widget, self::IGNORED)) {
                require_once self::WIDGETS_FOLDER . $widget;

                $class = str_replace('.php', '', $widget);
                $this->classConnector($class);
            }
        }
    }

    /**
     * @param $className
     */
    function classConnector($className)
    {

        $classNameWithNamespace = __NAMESPACE__ . '\Widgets\\' . $className;

        if (isset($this->classConnector[$className])) {
            $this->elementorHelper->getElementorPlugin()->widgets_manager->register_widget_type(new $classNameWithNamespace([], $this->classConnector[$className]));
        } else {
            $this->elementorHelper->getElementorPlugin()->widgets_manager->register_widget_type(new $classNameWithNamespace());
        }
    }
}