<?php

namespace Webselect\Webp\Helper;

/**
 * Class WebpConvert
 * @package webselect\Webp\Helper
 */
class WebpConvert
{
    /**
     * @param $image
     */
    function createWebp($image)
    {
        if ($this->isPng($image)) {
            $this->createWebpFromPng($image);
        } else {
            $this->createWebpFromJpg($image);
        }
    }

    /**
     * @param $originImageFile
     */
    function createWebpFromPng($originImageFile)
    {
        $img = imagecreatefrompng($originImageFile);
        imagepalettetotruecolor($img);
        imagealphablending($img, true);
        imagesavealpha($img, true);
        imagewebp($img, $this->getDirName($originImageFile) . '/' . $this->getFileName($originImageFile) . '.webp', 100);
        imagedestroy($img);
    }

    /**
     * @param $originImageFile
     */
    function createWebpFromJpg($originImageFile)
    {
        $image = imagecreatefromjpeg($originImageFile);
        ob_start();
        imagejpeg($image, NULL, 100);
        $cont = ob_get_contents();
        ob_end_clean();
        imagedestroy($image);
        $content = imagecreatefromstring($cont);
        imagewebp($content, $this->getDirName($originImageFile) . '/' . $this->getFileName($originImageFile) . '.webp');
        imagedestroy($content);
    }

    /**
     * @param $file
     * @return mixed|string
     */
    function getFileName($file)
    {
        return pathinfo($file)['filename'];
    }

    /**
     * @param $file
     * @return mixed|string
     */
    function getBaseName($file)
    {
        return pathinfo($file)['basename'];
    }

    /**
     * @param $file
     * @return mixed|string
     */
    function getDirName($file)
    {
        return pathinfo($file)['dirname'];
    }

    /**
     * @param $img
     * @return bool
     */
    function isPng($img)
    {
        return exif_imagetype($img) == IMAGETYPE_PNG;
    }

    /**
     * @param $img
     * @return bool
     */
    function isWebp($img)
    {
        return strpos($img, '.webp') !== false;
    }
}