<?php

namespace Webselect\Webp;

use Webselect\Webp\Helper\WebpConvert;

/**
 * Class Webp
 * @package webselect\Webp
 */
class Webp
{

    /**
     * @var WebpConvert
     */
    protected $webpConvert;

    /**
     * Webp constructor.
     * @param WebpConvert $webpConvert
     */
    function __construct(
        WebpConvert $webpConvert
    )
    {
        $this->webpConvert = $webpConvert;
        $this->uploadListener();
    }

    /**
     *
     */
    function uploadListener()
    {
        add_filter('wp_handle_upload', [$this, 'onWpImageUpload'], 10, 1);
        add_filter('wp_delete_file', [$this, 'onWpImageDelete'], 10, 1);
        add_filter('wp_generate_attachment_metadata', [$this, 'onWpGenerateSizes'], 15, 1);
    }

    /**
     * @param $metadata
     * @return mixed
     */
    function onWpGenerateSizes($metadata)
    {
        if (!isset($metadata['sizes'])) {
            return $metadata;
        }

        $uploadDir = wp_upload_dir()['path'];
        $subDir = wp_upload_dir()['subdir'];

        $sizes = $metadata['sizes'];

        $metadata['fileWebp'] = ltrim($subDir, '/') . '/' . $this->webpConvert->getFileName($metadata['file']) . '.webp';

        foreach ($sizes as $sizeType => $size) {
            $file = $size['file'];

            $metadata['sizes'][$sizeType]['fileWebp'] = $this->webpConvert->getFileName($file) . '.webp';

            $this->webpConvert->createWebp($uploadDir . '/' . $file);
        }

        return $metadata;
    }

    /**
     * @param $upload
     * @return mixed
     */
    function onWpImageUpload($upload)
    {
        $this->webpConvert->createWebp($upload['file']);

        return $upload;
    }

    /**
     * @param $file
     * @return mixed
     */
    function onWpImageDelete($file)
    {
        $baseDir = $this->webpConvert->getDirName($file);
        $fileName = $this->webpConvert->getFileName($file);
        $webP = $baseDir . '/' . $fileName . '.webp';
        if (file_exists($webP)) {
            unlink($webP);
        }

        return $file;
    }
}