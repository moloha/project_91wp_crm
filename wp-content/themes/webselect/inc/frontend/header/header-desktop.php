<div class="row">
<!--   container-big -->
    <div class="container">
        <div class="header-content">
            <div class="logo">
                <a href="<?php echo get_home_url(); ?>">
<!--                    --><?php //$exampleGroup = get_field('group
//                    -name', 'options');
//                    echo $exampleGroup['element']
//                    ?>
                    <img src="<?= getOptionField('header_logo'); ?>" alt="<?= get_the_title(); ?>"/>
                </a>
            </div>
            <div class="right-side">
                <div class="main-menu">
                    <?php wp_nav_menu(array(
                        'theme_location' => 'header_menu'
                    )); ?>
                </div>
<!--                <div class="contact-button">-->
<!--                    <a class="button-secondary" href="--><?//= getOptionField('contact_us_button')['url']; ?><!--"-->
<!--                       target="--><?//= getOptionField('contact_us_button')['target']; ?><!--">-->
<!--                        --><?//= getOptionField('contact_us_button')['title']; ?>
<!--                    </a>-->
<!--                </div>-->
            </div>
        </div>
    </div>
</div>