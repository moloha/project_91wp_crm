


<footer class="footer-wrapper">
    <div class="footer-content">
        <div class="footer-content_text">
            Copyright © 2019 Scutum Admin
        </div>

        <div class="footer-content_social-wrapper">
            <div class="footer-content_social-wrapper-text">
                Follow Us on
            </div>

            <div class="footer-content_social-wrapper-icon">
                <a href="#"><img src="<?= getOptionField('footer_social1'); ?> " width="20" height="20"></a>
                <a href="#"><img src="<?= getOptionField('footer_social2'); ?> " width="20" height="20"></a>
                <a href="#"><img src="<?= getOptionField('footer_social3'); ?> " width="20" height="20"></a>
            </div>

        </div>
    </div>

    <div class="delimiter"></div>

        <nav class="footer-contact">
            <ul>
                <li><a href="<?= getOptionField('footer_link1'); ?>">Terms of Use</a></li>
                <li><a href="<?= getOptionField('footer_link2'); ?>">Privacy Policy</a></li>
                <li><a href="<?= getOptionField('footer_link3'); ?>">Cookie Setting</a></li>
                <li><a href="<?= getOptionField('footer_link4'); ?>">About Us</a></li>
                <li><a href="<?= getOptionField('footer_link5'); ?>">Advertise with us</a></li>
                <li><a href="<?= getOptionField('footer_link6'); ?>">Newsletters</a></li>
                <li><a href="<?= getOptionField('footer_link7'); ?>">Sitemap</a></li>
            </ul>
        </nav>
</footer>