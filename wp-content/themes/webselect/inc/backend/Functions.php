<?php

function getOptionField($name) {
    return get_field($name, 'option');
}