<?php get_template_part('inc/frontend/header/global-header'); ?>

<div class="header">
    <div class="header-desktop hide-mobile">
        <?php get_template_part('inc/frontend/header/header-desktop'); ?>
    </div>

    <div class="header-mobile hide-desktop">
        <?php get_template_part('inc/frontend/header/header-mobile'); ?>
    </div>
</div>

