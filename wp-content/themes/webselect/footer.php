<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Engitech
 */

?>
<?php get_template_part('inc/frontend/footer/global-footer'); ?>
	</div><!-- #content -->
	<?php
		if ( ! function_exists( 'elementor_theme_do_location' ) || ! elementor_theme_do_location( 'footer' ) ) {
			get_template_part( 'template-parts/content', 'footer' );
		}
	?>
</div><!-- #page -->

<div class="footer">
    <div class="footer-desktop hide-mobile">
        <?php get_template_part('inc/frontend/footer/footer-desktop'); ?>
    </div>

    <div class="footer-mobile hide-desktop">
        <?php get_template_part('inc/frontend/footer/footer-mobile'); ?>
    </div>
</div>

<?php wp_footer(); ?>

</body>
</html>
