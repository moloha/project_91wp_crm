<?php
$settings = $args['settings'];
wp_enqueue_script('random-people', JS_PATH . '/random-people.js', ['jquery', PROJECT_KEY . '-functions'], '1.0.0', true);
wp_enqueue_script('random-dot-color', JS_PATH . '/random-dot-color.js', ['jquery', PROJECT_KEY . '-functions'], '1.0.0', true);
?>

<div class="top-banner">
    <div class="row">
        <div class="container-big">
            <div class="top-banner-content">
                <div class="top-banner-title-section">
                    <h3><?= $settings['sub_title']; ?></h3>
                    <h1><?= $settings['title']; ?></h1>
                    <p><?= $settings['description']; ?></p>
                    <a href="<?= $settings['button']['url']; ?>"
                       class="button-main"<?= ($settings['button']['is_external']) ? 'target="_blank"' : ''; ?>><?= $settings['button_title']; ?></a>
                </div>
                <div class="top-banner-image-section">
                    <div class="people-section image-section_1_people">
                        <div class="people image-section_1_people_1"></div>
                        <div class="people image-section_1_people_2"></div>
                        <div class="people image-section_1_people_3"></div>
                        <div class="people image-section_1_people_4"></div>
                    </div>

                    <div class="people-section people-section_2_people">
                        <div class="people image-section_2_people_1"></div>
                    </div>

                    <div class="people-section people-section_3_people">
                        <div class="people image-section_3_people_1"></div>
                    </div>

                    <div class="image-section image-section_1">
                        <div class="dot dot_1"></div>
                        <div class="dot dot_2"></div>
                        <div class="dot dot_3"></div>

                        <div class="image-section image-section_2">
                            <div class="dot dot_1"></div>
                            <div class="dot dot_2"></div>

                            <div class="image-section image-section_3">
                                <div class="dot dot_1"></div>
                                <div class="dot dot_2"></div>
                            </div>
                        </div>
                    </div>
                    <div class="image-section-content">
                        <h4><?= $settings['slogan']; ?></h4>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="facebook-outline">
        <div class="facebook-outline-blue"></div>
        <div class="facebook-outline-dark-blue"></div>
    </div>
</div>