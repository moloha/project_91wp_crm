<?php
$settings = $args['settings'];
?>

<div class="wrapper-price ">
    <div class="price-section-content">
        <div class="price-table-title">
			<?= $settings['title']; ?>
        </div>

        <div class="price-sub_title">
            <div class="price-sub_title-price"> <?= $settings['price']; ?></div>
            <div class="price-sub_title-description">/Net/user/month</div>
        </div>

        <div class="price-table_field">
            <div class="pr">&#10003;</div>
            <div class="pr_ar">
				<?= $settings['sub_pricetitle']; ?>
            </div>
        </div>

        <!--  form a table with data -->

		<?php foreach ( $settings['my_table_fields'] as $item ): ?>
            <div class="price-table_field">
                <div class="pr">&#10003;</div>
                <div class="pr_ar">
		            <?= $item['sub_pricetitle']; ?>
                </div>
            </div>
		<?php endforeach; ?>

        <!-- end of forming tables with data -->
    </div>

    <!--    <div class="price_button-wrapper">-->
    <!--		<div class="button-solution">-->
    <!--			<a href="--><? //= $settings['button']['url']; ?><!--"-->
    <!--			   class=""--><? //= ($settings['button']['is_external']) ? 'target="_blank"' : ''; ?>
    <!--				--><? //= $settings['button_title']; ?>
    <!--			</a>-->
    <!--		</div>-->
    <!--    </div>-->

</div>
