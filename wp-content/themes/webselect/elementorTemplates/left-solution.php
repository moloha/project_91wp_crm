<?php
$settings = $args['settings'];
?>

<div class="wrapper-solution title-section position-<?= $settings['position'] ?>">
    <div class="solution-section-content">

        <div class="solution-title">
            <h2><?= $settings['title']; ?></h2>
            <div class="solution-sub_title"><?= $settings['sub_title']; ?></div>
        </div>

        <!--		--><?php //if (!empty($settings['description'])): ?>
        <div class="solution-description"><?= $settings['description']; ?></div>
        <!--		--><?php //endif; ?>

        <div class="button-solution">
            <a href="<?= $settings['button']['url']; ?>"
               class=""<?= ( $settings['button']['is_external'] ) ? 'target="_blank"' : ''; ?>>
				<?= $settings['button_title']; ?>
            </a>
        </div>

    </div>
</div>