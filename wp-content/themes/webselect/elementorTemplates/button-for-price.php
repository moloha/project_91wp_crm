<?php
$settings = $args['settings'];
?>

<div class="title-section position-<?= $settings['position'] ?>">
    <div class="solution-section-content">


        <div class="button-solution">
            <a href="<?= $settings['button']['url']; ?>"
               class=""<?= ( $settings['button']['is_external'] ) ? 'target="_blank"' : ''; ?>>
				<?= $settings['button_title']; ?>
            </a>
        </div>

    </div>
</div>