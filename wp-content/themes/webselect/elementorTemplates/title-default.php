<?php
$settings = $args['settings'];
?>

<div class="title-section position-<?= $settings['position'] ?>">
    <div class="title-section-content">
        <div class="title"><?= $settings['title']; ?></div>
        <?php if (!empty($settings['description'])): ?>
            <div class="description"><?= $settings['description']; ?></div>
        <?php endif; ?>
    </div>
</div>