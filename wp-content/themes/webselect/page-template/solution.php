<?php /* Template Name: Solution */ ?>

<?php get_header();?>


<div class="image-wrapper">
	<img src="<?= getOptionField('banner_solution'); ?>"/>
</div>
<div class="wrapper-container">
<section class="solution-container">

<!--	<div class="">-->
<!--		<h1>this is the test solution page</h1>-->
<!--	</div>-->

	<?php the_content(); ?>
</section>

</div>

<?php get_footer();?>
